#!/bin/sh

echo -e "Starting payment consumer..."
python consumer_payment.py & 

status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start consumer payment: $status"
      exit $status
fi
echo -e "Payment consumer started [OK]"

echo -e "Starting gunicorn..."
gunicorn -w 2 -b 0.0.0.0:5000 app:app 

status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start gunicorn: $status"
      exit $status
fi
echo -e "Gunicorn started [OK]"

