import os
import time
from kombu import Connection, Consumer, Exchange, Queue, Consumer, Producer
from pymongo import MongoClient
from bson.objectid import ObjectId



# Rabbitmq
#rabbit_url = "amqp://rabbitmq:5672"
RABBITMQ_URL = os.environ.get("RABBITMQ_URL")
conn = Connection(RABBITMQ_URL)
exchange = Exchange("orders", type="direct")
queue = Queue(name="payment", exchange=exchange, routing_key="payment")


# Mongodb
MONGODB_URL = os.environ.get("MONGODB_URL")
client = MongoClient(MONGODB_URL)
puc_store_db = client['pucstore']


def update_order(order_message):
    collection = puc_store_db.orders
    
    order_id = order_message['order_id']
    order_status = order_message['status']

    query = {'_id': ObjectId(order_id)}
    update_status = { "$set" : { "status" : order_status}}
    print(f"order to store {order_message}")
    
    res = collection.update_one(query, update_status)

    print(res.matched_count)



def proccess_message(body, message):
    print(f"paymante proccess: {body}")
    message.ack()
    update_order(body)



def start_payment_consumer():
    with Consumer(
        conn,
        queues=queue,
        callbacks=[proccess_message],
        accept=["text/plain", "application/json"]
    
    ):
        while True:
            try:
                conn.drain_events()
            except Exception as e:
                print(e)

            time.sleep(1)



if __name__ == "__main__":
    start_payment_consumer()
