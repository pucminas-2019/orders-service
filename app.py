import os, sys
import logging 
import datetime
import requests 
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_cors import CORS
from pymongo import MongoClient
from bson.json_util import dumps
from kombu import Connection, Exchange, Queue, Producer


file_handler = logging.FileHandler('app.log')

#rabbitmq_url = "amqp://rabbitmq:5672"
RABBITMQ_URL = os.environ.get('RABBITMQ_URL')
conn = Connection(RABBITMQ_URL)

try:
    channel = conn.channel()
except Exception as ex:
    print(f'Falha na conexão com : {RABBITMQ_URL}')
    sys.exit(1) 

exchange = Exchange("orders", type="direct")
producer = Producer(exchange=exchange, channel=channel, routing_key="orders")
queue = Queue(name="orders", exchange=exchange, routing_key="orders")
queue.maybe_bind(conn)
queue.declare()

MONGODB_URL = os.environ.get('MONGODB_URL') 
client = MongoClient(MONGODB_URL)
#client = MongoClient('localhost', 49155)
puc_store_db = client['pucstore']

app = Flask(__name__)
CORS(app)
api = Api(app, prefix='/api/v1')

app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)


def add_order_status(order: dict) -> dict:
    order['status'] = "Em Aprovação"
    return order


def add_order_datetime(order: dict) -> dict:
    order['created_at'] = datetime.datetime.utcnow()
    return order


def total_order(products: list, lookup: str) -> int:
    return sum([p[lookup] for p in products])


def create_order(order: dict) -> dict:
    order = order
    order = add_order_datetime(order)
    order = add_order_status(order)
    order['total'] = total_order(order.get('products'), 'total')
    return order
    

def save_order(order: dict) -> tuple: 
    collection = puc_store_db.orders
    res = collection.insert_one(order)
    return res.acknowledged, res.inserted_id


def get_orders(user_id: str) -> list: 
    collection = puc_store_db.orders
    cursor = collection.find({}, {'_id': False})
    return [order for order in cursor] 


class OrdersApiView(Resource):
    def get(self):
        user_orders = get_orders('teste')
        return jsonify({'orders': user_orders})
        

    def post(self):
        resp = request.get_json(force=True)
        if resp and 'data' in resp:
            app.logger.info('Receiving Orders')
            data = resp['data']
            # put orders into store 
            order = create_order(data)
            saved, order_id = save_order(order)
            # put orders into queue
            
            if not saved:
                return jsonify({"message": "orders problem"})
            
            msg_queue = {
                "order_id": str(order_id), 
                "amount": order["total"],
            }
            app.logger.info('Sending orders to queue')
            producer.publish(msg_queue)
            
            return jsonify({'orders': True})


        return jsonify({'orders': 'no orders reveived'})



api.add_resource(OrdersApiView, '/orders')

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
